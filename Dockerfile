FROM ekidd/rust-musl-builder:latest

ADD extra-cargo-conf /

RUN sudo apt-get update && sudo apt-get install -y \
	gcc-mingw-w64 libcurl4-openssl-dev libelf-dev libdw-dev build-essential cmake make wget python python3 lzma-dev && \
	sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/* && \
	rustup target add x86_64-pc-windows-gnu && \
    rustup target add i686-pc-windows-gnu && \
	sudo bash -c 'cat /extra-cargo-conf >> ~/.cargo/config && rm /extra-cargo-conf' && \
    wget -c https://github.com/SimonKagstrom/kcov/archive/master.tar.gz && \
    tar xzf master.tar.gz && mkdir kcov-master/build && cd kcov-master/build && cmake .. && make && \
    sudo make install && cd ../..

